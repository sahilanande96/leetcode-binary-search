"""
Given an array of integers nums sorted in ascending order, find the starting and ending position of a given target value.

If target is not found in the array, return [-1, -1].

Follow up: Could you write an algorithm with O(log n) runtime complexity?

 

Example 1:

Input: nums = [5,7,7,8,8,10], target = 8
Output: [3,4]
Example 2:

Input: nums = [5,7,7,8,8,10], target = 6
Output: [-1,-1]
Example 3:

Input: nums = [], target = 0
Output: [-1,-1]
 

Constraints:

0 <= nums.length <= 105
-109 <= nums[i] <= 109
nums is a non-decreasing array.
-109 <= target <= 109
"""

# Code
class Solution:
    def searchRange(self, nums: List[int], target: int) -> List[int]:
       
        start, end = 0, len(nums) - 1
        
        if len(nums) == 0:
            return([-1,-1])
        
        if (nums[start] == target and nums[end] == target):
            return([start, end])

        loop = 0
        
        start, end = 0, len(nums) - 1
        while start + 1 < end:
            loop+= 1
            mid = (start + end) // 2
            # print("Loop : ", loop)
            # print("Start is ", start)
            # print("End is ", end)
            # print("mid is ", mid)
            if nums[mid] == target:
                if ((nums[mid -1 ] != target) or (nums[start] != target)):
                    start += 1
                    # print("Still finding start")
                elif nums[end] != target:
                    end -= 1
                    # print("Still finding end")
                elif ((nums[start] == target) and (nums[end] == target)):
                    return[start, end]
                # end = mid + 1
                # print("Start becomes mid + 1 ", start)
                # print("End becomes mid - 1", end)
            elif nums[mid] < target:
                start = mid
                # print("Start::: " ,start)
            else:
                end = mid
                # print("end ::: ", end )

        # Post-processing:
        # End Condition: left + 1 == right
        if ((nums[start] == target) and (nums[end] == target)):
            return[start, end]
        elif nums[start] == target:
            return[start, start]
        elif nums[end] == target:
            return[end,end]
        return([-1, -1])
            

"""
88 / 88 test cases passed.
Status: Accepted
Runtime: 88 ms
Memory Usage: 15.4 MB
"""
