"""
There is an integer array nums sorted in ascending order (with distinct values).

Prior to being passed to your function, nums is rotated at an unknown pivot index k (0 <= k < nums.length) such that the resulting array is [nums[k], nums[k+1], ..., nums[n-1], nums[0], nums[1], ..., nums[k-1]] (0-indexed). For example, [0,1,2,4,5,6,7] might be rotated at pivot index 3 and become [4,5,6,7,0,1,2].

Given the array nums after the rotation and an integer target, return the index of target if it is in nums, or -1 if it is not in nums.

 

Example 1:

Input: nums = [4,5,6,7,0,1,2], target = 0
Output: 4
Example 2:

Input: nums = [4,5,6,7,0,1,2], target = 3
Output: -1
Example 3:

Input: nums = [1], target = 0
Output: -1
 

Constraints:

1 <= nums.length <= 5000
-104 <= nums[i] <= 104
All values of nums are unique.
nums is guaranteed to be rotated at some pivot.
-104 <= target <= 104
 

Follow up: Can you achieve this in O(log n) time complexity?
"""

#Code
class Solution:
    def search(self, nums: List[int], target: int) -> int:
        start = 0
        end = len(nums) - 1
        
        while(start <= end):
            
            mid = ((end -start)//2) + start
            
            if(mid >= len(nums)):
                return(-1)
            
            if nums[mid] == target:
                return(mid)
            elif nums[start] <= nums[mid]:
                if (target >= nums[start]) and (target <= nums[mid]):
                    end = mid -1
                else:
                    start = mid + 1
            elif nums[end] >= nums[mid]:
                if (target <= nums[end]) and (target >= nums[mid]):
                    start = mid + 1
                else:
                    end = mid - 1
            
        return(-1)


"""
195 / 195 test cases passed.
Status: Accepted
Runtime: 32 ms
Memory Usage: 14.6 MB
"""
