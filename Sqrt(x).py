"""
Given a non-negative integer x, compute and return the square root of x.

Since the return type is an integer, the decimal digits are truncated, and only the integer part of the result is returned.

 

Example 1:

Input: x = 4
Output: 2
Example 2:

Input: x = 8
Output: 2
Explanation: The square root of 8 is 2.82842..., and since the decimal part is truncated, 2 is returned.
 

Constraints:

0 <= x <= 231 - 1

"""

# Code
class Solution:
    def mySqrt(self, x: int) -> int:
        start = 0
        end = x
        
        if start == x:
            return(x)
        
        while (start <= end):
            mid = (end -start)//2 + start
            if mid**2 == x :
                return(mid)
            elif ((mid**2) < x):
                start = mid + 1 
            elif ((mid**2) > x):
                end = mid - 1
                
        return(end)


"""

1017 / 1017 test cases passed.
Status: Accepted
Runtime: 52 ms
Memory Usage: 14.3 MB
"""
